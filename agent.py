from game2048_visualizer import Game2048Visualizer
import copy
import random
import math
class Agent:
    def __init__(self):
        pass

    def evaluate_board(self, board):
        # Calculate the value of the board based on the score, big tiles in corners,
        # close tiles, empty tiles, and monotonicity
        score = sum(value for row in board for value in row)
        max_tile = max(max(row) for row in board)
        corner_tiles = [board[0][0], board[0][-1], board[-1][0], board[-1][-1]]
        big_corner_tiles = [tile for tile in corner_tiles if tile >= 64]
        close_tiles = 0
        empty_tiles = 0
        big_tile_closeness = 0
        monotonicity = 0
        for i in range(len(board)):
            for j in range(len(board)):
                if board[i][j] == 0:
                    empty_tiles += 1
                else:
                    if i > 0 and board[i-1][j] == board[i][j]:
                        close_tiles += board[i][j]
                        if math.log2(board[i][j]) - math.log2(board[i-1][j]) == 1:
                            big_tile_closeness += board[i][j]
                    if i < len(board)-1 and board[i+1][j] == board[i][j]:
                        close_tiles += board[i][j]
                        if math.log2(board[i][j]) - math.log2(board[i+1][j]) == 1:
                            big_tile_closeness += board[i][j]
                    if j > 0 and board[i][j-1] == board[i][j]:
                        close_tiles += board[i][j]
                        if math.log2(board[i][j]) - math.log2(board[i][j-1]) == 1:
                            big_tile_closeness += board[i][j]
                    if j < len(board)-1 and board[i][j+1] == board[i][j]:
                        close_tiles += board[i][j]
                        if math.log2(board[i][j]) - math.log2(board[i][j+1]) == 1:
                            big_tile_closeness += board[i][j]
            if i > 0:
                monotonicity += sum(abs(board[i][j] - board[i-1][j]) for j in range(len(board)))

        return score + len(big_corner_tiles) * 1000 + close_tiles + big_tile_closeness * math.log2(max_tile) - monotonicity * 5 + empty_tiles * 200

    def get_best_move(self, game, depth=5, random_moves=False):
        moves = [move for move in ['up', 'down', 'left', 'right'] if game.can_move_tiles(move)]
        scores = []
        if not moves:
            scores.append(('end', 0))
        if random_moves:
            return random.choice(moves)
        for move in moves:
            new_game = copy.deepcopy(game)
            new_game.next_state(move)
            if depth == 1:
                score = self.evaluate_board(new_game.board)
            else:
                score = self.get_best_move(new_game, depth=depth-1)
            scores.append((move, score))
        scores.sort(key=lambda x: x[1], reverse=True)
        return scores[0][0]

    def train(self, nb_game):
        results = []
        game = Game2048Visualizer()
        game.run(self, nb_game, results)
