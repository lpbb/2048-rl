# Reinforcement Learning Introduction Project: 2048 Game Agent

This project is an introduction to reinforcement learning concepts by implementing an intelligent agent to play the popular 2048 game. The main goal of the project is to provide an understanding of how reinforcement learning can be applied to create an agent that can learn and make decisions in a game environment.

## 2048 Game Rules

The 2048 game is a single-player sliding tile puzzle game. The game is played on a 4x4 grid, and the objective is to combine the numbered tiles to create a tile with the number 2048. The player can slide the tiles in four directions: up, down, left, or right. When two tiles with the same number collide, they merge into a single tile with their combined value. The game ends when there are no more valid moves or when the 2048 tile is achieved.

## Reinforcement Learning and the Agent

Reinforcement learning is a type of machine learning where an agent learns to make decisions by interacting with an environment. The agent receives feedback in the form of rewards or penalties and adjusts its behavior to maximize the cumulative reward.

In this project, we have implemented an agent that can play the 2048 game using a simple reinforcement learning approach. The agent evaluates the game board and decides on the best move to make based on an evaluation function. The agent uses the evaluation function to assign a score to each possible move and selects the move with the highest score.

The evaluation function considers various factors such as:

- The total score of the board
- The number of big tiles (value >= 64) in the corners
- The number of close tiles (adjacent tiles with the same value)
- The number of empty tiles
- Monotonicity (smooth transition between tile values)

These factors are combined with appropriate weights to calculate a final score for each possible move. The agent then chooses the move with the highest score.

The agent also employs a search algorithm with a specified depth to look ahead and anticipate the outcomes of potential moves. This allows the agent to make more informed decisions and achieve higher scores in the game.

## Results

The two following plots present the final game score for two different strategies. First the ramdom behaviour, then our agent.  
Ramdom behaviour:  
![Random behaviour agent](plot_random.png)  
Our Agent:  
![Our agent](plot_agent.png)  


## Usage

```bash
git clone https://gitlab.com/lpbb/2048-rl.git
cd 2048-rl
pip install -r requirements.txt
python main.py
```

The `Game2048Visualizer` will visualize the agent's gameplay and display the results.

## Authors

Chapron Antoine  
Kebrit Badr-eddine  
Pre Fabien  
Perraud Louis
