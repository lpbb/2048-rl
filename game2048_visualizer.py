import pygame
from game2048 import Game2048
import matplotlib.pyplot as plt

class Game2048Visualizer:
    def __init__(self, game_size=4, tile_size=100, margin=10):
        self.game_size = game_size
        self.tile_size = tile_size
        self.margin = margin
        self.game = Game2048(game_size)
        self.width = game_size * tile_size + (game_size + 1) * margin
        self.height = self.width
        pygame.init()
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption('2048 Game')
        self.font = pygame.font.SysFont('Arial', 32)

    def run(self, agent, nb_game,results):
        nb_game_played = 0
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
            # Get agent's action
            action = agent.get_best_move(self.game,random_moves = False)
            # Perform action
            self.game.next_state(action)
            self.draw_board()
            if self.game.is_game_over():
                nb_game_played += 1
                results.append((self.game.max_tile(),self.game.score))
                self.draw_endgame_screen()
                pygame.display.update()
                # pygame.time.wait(300)  
                self.game = Game2048(self.game_size)
            else:
                pygame.display.update()
            if nb_game_played >= nb_game :
                plt.plot(results)
                plt.savefig("plot_new_rl.png")
                plot_image = pygame.image.load("plot_new_rl.png")
                pygame.display.set_mode((700, 500))
                self.screen.blit(plot_image, (0, 0))
                pygame.display.update()
                pygame.time.wait(10000)
                pygame.quit()
                quit()

    def draw_board(self):
        self.screen.fill((255, 255, 255))
        for i in range(self.game_size):
            for j in range(self.game_size):
                tile = self.game.board[i][j]
                x = j * (self.tile_size + self.margin) + self.margin
                y = i * (self.tile_size + self.margin) + self.margin
                pygame.draw.rect(self.screen, self.get_tile_color(tile), (x, y, self.tile_size, self.tile_size))
                if tile != 0:
                    text = self.font.render(str(tile), True, (255, 255, 255))
                    text_rect = text.get_rect(center=(x + self.tile_size / 2, y + self.tile_size / 2))
                    self.screen.blit(text, text_rect)

    def draw_endgame_screen(self):
        score = self.game.current_score()
        endgame_font = pygame.font.SysFont('Arial', 20)
        text = endgame_font.render(f"Game Over! Score: {score}", True, (0, 0, 0))
        text_rect = text.get_rect(center=(self.width/3, self.height/4))
        self.screen.fill((255, 255, 255))
        self.screen.blit(text, text_rect)

    def get_tile_color(self, value):
        colors = {
            0: (204, 192, 179),
            2: (238, 228, 218),
            4: (237, 224, 200),
            8: (242, 177, 121),
            16: (245, 149, 99),
            32: (246, 124, 95),
            64: (246, 94, 59),
            128: (237, 207, 114),
            256: (237, 204, 97),
            512: (237, 200, 80),
            1024: (237, 197, 63),
            2048: (237, 194, 46),
        }
        return colors.get(value, (255, 0, 0))