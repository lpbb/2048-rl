import random

class Game2048:
    def __init__(self, size=4):
        self.size = size
        self.board = [[0] * size for _ in range(size)]
        self.score = 0
        self.add_tile()
        self.add_tile()

    def add_tile(self):
        empty_cells = [(i, j) for i in range(self.size) for j in range(self.size) if self.board[i][j] == 0]
        if empty_cells:
            i, j = random.choice(empty_cells)
            self.board[i][j] = 2 if random.random() < 0.9 else 4

    def max_tile(self):
        return max(max(row) for row in self.board)

    def current_score(self):
        return self.score
    
    def can_move_tiles(self, direction):
        if direction == 'up':
            for j in range(self.size):
                for i in range(1, self.size):
                    if self.board[i][j] != 0 and (self.board[i-1][j] == 0 or self.board[i][j] == self.board[i-1][j]):
                        return True
        elif direction == 'down':
            for j in range(self.size):
                for i in range(self.size - 2, -1, -1):
                    if self.board[i][j] != 0 and (self.board[i+1][j] == 0 or self.board[i][j] == self.board[i+1][j]):
                        return True
        elif direction == 'left':
            for i in range(self.size):
                for j in range(1, self.size):
                    if self.board[i][j] != 0 and (self.board[i][j-1] == 0 or self.board[i][j] == self.board[i][j-1]):
                        return True
        elif direction == 'right':
            for i in range(self.size):
                for j in range(self.size - 2, -1, -1):
                    if self.board[i][j] != 0 and (self.board[i][j+1] == 0 or self.board[i][j] == self.board[i][j+1]):
                        return True
        return False

    def next_state(self, direction):
        if direction == 'up':
            self.transpose()
            self.move_left()
            self.transpose()
        elif direction == 'down':
            self.transpose()
            self.move_right()
            self.transpose()
        elif direction == 'left':
            self.move_left()
        elif direction == 'right':
            self.move_right()
        self.add_tile()

    def move_left(self):
        for i in range(self.size):
            row = self.board[i]
            row, score = self.compress(row)
            row, score = self.merge(row, score)
            row, _ = self.compress(row)
            self.board[i] = row
            self.score += score

    def move_right(self):
        for i in range(self.size):
            row = self.board[i][::-1]
            row, score = self.compress(row)
            row, score = self.merge(row, score)
            row, _ = self.compress(row)
            self.board[i] = row[::-1]
            self.score += score

    def merge(self, row, score):
        for i in range(self.size - 1):
            if row[i] == row[i+1] and row[i] != 0:
                row[i] *= 2
                row[i+1] = 0
                score += row[i]
        return row, score

    def compress(self, row):
        new_row = [i for i in row if i != 0]
        zeros = [0] * (self.size - len(new_row))
        new_row += zeros
        return new_row, 0

    def transpose(self):
        self.board = [list(x) for x in zip(*self.board)]

    def display(self):
        for i in range(self.size):
            for j in range(self.size):
                print(self.board[i][j], end='\t')
            print()

    def is_game_over(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.board[i][j] == 0:
                    return False
                if j < self.size - 1 and self.board[i][j] == self.board[i][j+1]:
                    return False
                if i < self.size - 1 and self.board[i][j] == self.board[i+1][j]:
                    return False
        return True
